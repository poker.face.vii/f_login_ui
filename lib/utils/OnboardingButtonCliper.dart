import 'package:flutter/material.dart';

class OnboardingButtonCliper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.moveTo(size.width, 0);
    path.quadraticBezierTo(-20, size.height-40, size.width, size.height-40);
    path.lineTo(130, size.height-60);
    
    path.quadraticBezierTo(-15, size.height-95, size.width, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
  

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
