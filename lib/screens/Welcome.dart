import 'package:flutter/material.dart';
import 'package:tat_ui_login/screens/login.dart';
import 'package:tat_ui_login/widgets/onBoardingButton.dart';
import '../utils/omBoardingImageCliper.dart';
import '../widgets/onBoardingButton.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: Column(
        children: <Widget>[
          ClipPath(
            clipper: OnboardingImageCliper(),
            child: Container(
              width: double.infinity,
              height: 500,
              child: Stack(
                children: <Widget>[
                  Positioned(
                    left: 0,
                    top: -2,
                    bottom: -150,
                    child: Image.asset(
                      "assets/img/owl-bg.jpg",
                      fit: BoxFit.cover,
                      width: 450,
                      height: 2000,
                    ),
                  ),
                  Positioned(
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: Container(
                      width: double.infinity,
                      height: 340,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Color(0xffe2b0ff).withOpacity(.8),
                            Color(0xff9f44d3).withOpacity(.05),
                          ],
                          begin: Alignment.bottomCenter,
                          end: Alignment.topCenter,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                      right: 34.5,
                      top: 58,
                      child: Text(
                        "POK",
                        style: TextStyle(
                          fontSize: 45,
                          color: Colors.white,
                          fontFamily: "NXT",
                        ),
                      )),
                  Positioned(
                    top: 94,
                    right: 16,
                    child: Text(
                      "FAC",
                      style: TextStyle(
                          fontSize: 45, color: Colors.white, fontFamily: "NXT"),
                    ),
                  )
                ],
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 32.0),
                child: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: "Tattoo",
                        style: TextStyle(
                          fontSize: 33,
                          color: Colors.black,
                          fontFamily: "NXT",
                        ),
                      ),
                      TextSpan(
                        text: "Studio",
                        style: TextStyle(
                            fontSize: 26,
                            color: Color(0xff4a4a4a),
                            fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),
                ),
              ),GestureDetector(onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (_)=> LoginPage()));},
                child: OnBoardingButton())
            ],
          )
        ],
      ),
    );
  }
}
