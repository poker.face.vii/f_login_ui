import 'package:flutter/material.dart';
import 'package:tat_ui_login/screens/Welcome.dart';
import 'package:tat_ui_login/utils/omBoardingImageCliper.dart';
import 'package:tat_ui_login/widgets/onBoardingButton.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                ClipPath(
                  clipper: OnboardingImageCliper(),
                  child: Container(
                    width: double.infinity,
                    height: 500,
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          right: 0,
                          left: 0,
                          top: -25,
                          child: Image.asset(
                            "assets/img/login-bg.jpg",
                            fit: BoxFit.cover,
                          ),
                        ),
                        Positioned(
                          left: 0,
                          right: 0,
                          bottom: 0,
                          child: Container(
                            width: double.infinity,
                            height: 350,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  Color(0xffe2b0ff).withOpacity(0.8),
                                  Color(0xff9f44d3).withOpacity(.05)
                                ],
                                begin: Alignment.bottomCenter,
                                end: Alignment.topCenter,
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          left: 40,
                          top: 50,
                          child: Text(
                            "NXT",
                            style: TextStyle(
                              fontSize: 40,
                              fontFamily: "NXT",
                            ),
                          ),
                        ),
                        Positioned(
                          left: 65,
                          top: 88,
                          child: Text(
                            "LVL",
                            style: TextStyle(
                              fontSize: 40,
                              fontFamily: "NXT",
                            ),
                          ),
                        ),
                        Positioned(
                          left: 30,
                          top: 132,
                          child: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: "Tattoo",
                                  style: TextStyle(
                                    color: Colors.blueGrey[900],
                                    fontWeight: FontWeight.bold,
                                    fontFamily: "NXT",
                                    fontSize: 18,
                                  ),
                                ),
                                TextSpan(
                                  text: " Studio",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    color: Colors.blueGrey[900],
                                    // fontFamily: "NXT",
                                    fontSize: 16,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  right: 30,
                  bottom: 0,
                  child: Text(
                    "LOGIN",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 40,
                      color: Color(0xffCBCBCB),
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 25,
            ),
            buildUsername(),
            SizedBox(
              height: 20,
            ),
            buildPassword(),
            SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 35),
                  child: FlatButton(
                      onPressed: null,
                      child: Text(
                        "SignUp",
                        style: TextStyle(fontSize: 18),
                      )),
                ),
                GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (_) => WelcomePage()));
                    },
                    child: OnBoardingButton())
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget buildUsername() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30),
      width: double.infinity,
      height: 58,
      decoration: BoxDecoration(
        color: Color(0xFFe7e7e7),
        borderRadius: BorderRadius.circular(40),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 4,
            offset: Offset(0, 4),
          ),
        ],
      ),
      child: Padding(
        padding: EdgeInsets.only(top: 4, left: 24, right: 16),
        child: TextField(
          decoration: InputDecoration(
              hintText: "Username",
              hintStyle: TextStyle(fontSize: 16),
              enabledBorder: InputBorder.none,
              suffixIcon: Icon(Icons.person)),
        ),
      ),
    );
  }

  Widget buildPassword() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30),
      width: double.infinity,
      height: 58,
      decoration: BoxDecoration(
        color: Color(0xFFe7e7e7),
        borderRadius: BorderRadius.circular(40),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 4,
            offset: Offset(0, 4),
          ),
        ],
      ),
      child: Padding(
        padding: EdgeInsets.only(top: 4, left: 24, right: 16),
        child: TextField(
          decoration: InputDecoration(
              hintText: "Password",
              hintStyle: TextStyle(fontSize: 16),
              enabledBorder: InputBorder.none,
              suffixIcon: Icon(Icons.remove_red_eye)),
        ),
      ),
    );
  }
}
