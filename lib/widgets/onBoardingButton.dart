import 'package:flutter/material.dart';
import '../utils/OnboardingButtonCliper.dart';

class OnBoardingButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: OnboardingButtonCliper(),
          child: Container(
        width: 135,
        height: 155,
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: [
            Color(0xff9f3cb2),
            Color(0xfff26567),
          ], stops: [
            0.4,
            1.0
          ]),
        ),
        child: Center(
          child: Padding(
            padding: EdgeInsets.only(left: 45),
            child: Padding(
              padding: const EdgeInsets.only(top: 12),
              child: Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
